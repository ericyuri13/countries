/// <reference types="Cypress" />
context('Method 1: Unit testing APEX', () => {
  const loginPage = 'https://apex.oracle.com/pls/apex/ericsacramento/r/demonstration-countries/login'
  const pUsername = Cypress.env('USERNAME')
  const pPassword = Cypress.env('PASSWORD')

  it('login page', () => {
    // cy.server()
    cy.intercept('POST', 'ords/wwv_flow.accept').as('login')
    cy.visit(loginPage)
    cy.get('#P9999_USERNAME')
    .clear()
    .should('be.empty')
    .type(pUsername)
    .should('have.value', pUsername)

    cy.get('#P9999_PASSWORD')
    .should('be.empty')
    .type(pPassword)
    .should('have.value', pPassword)

    cy.get('#btn-login').click()

    //Interactive Grid Card
    cy.get(':nth-child(4) > .t-Card > .t-Card-wrap > .t-Card-titleWrap').click()

    //Add New Button
    cy.get(':nth-child(6) > .a-Button').click()

    //Country Name
    cy.get('.is-inserted > :nth-child(3)').click({ force: true })
    cy.get('#country-name-col').type('Eric Yuri')
    cy.get('.a-GV-expandCollapse > .a-Icon').click()
    //

    //Nationality
    cy.get('.is-selected > .a-GV-cell:nth-child(4)').click()
    cy.get('#nationality-col').type('BBBBB')
    cy.get('.a-GV-expandCollapse > .a-Icon').click()

    //Save button
    cy.get(':nth-child(5) > .js-actionButton').click()
  })

  Cypress.on('uncaught:exception', (err, runnable) => {
    return false
  })
})
