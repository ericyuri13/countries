/// <reference types="Cypress" />
context('Method 1: Unit testing APEX', () => {
  const loginPage = 'https://apex.oracle.com/pls/apex/ericsacramento/r/demonstration-countries/login'
  const pUsername = Cypress.env('USERNAME')
  const pPassword = Cypress.env('PASSWORD')

  it('login page', () => {
    // cy.server()
    cy.intercept('POST', 'ords/wwv_flow.accept').as('login')
    cy.visit(loginPage)
    cy.get('#P9999_USERNAME')
    .clear()
    .should('be.empty')
    .type(pUsername)
    .should('have.value', pUsername)

    cy.get('#P9999_PASSWORD')
    .should('be.empty')
    .type(pPassword)
    .should('have.value', pPassword)

    cy.get('#btn-login').click()

    cy.get(':nth-child(1) > .t-Card > .t-Card-wrap > .t-Card-titleWrap').click()
  })

  Cypress.on('uncaught:exception', (err, runnable) => {
    return false
  })
})
