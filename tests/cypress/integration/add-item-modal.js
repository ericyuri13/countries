context('Method 1: Unit testing APEX', () => {
  const loginPage = 'https://apex.oracle.com/pls/apex/ericsacramento/r/demonstration-countries/login'
  const pUsername = Cypress.env('USERNAME')
  const pPassword = Cypress.env('PASSWORD')

  it('login page', () => {
    // cy.server()
    cy.intercept('POST', 'ords/wwv_flow.accept').as('login')
    cy.visit(loginPage)
    cy.get('#P9999_USERNAME')
    .clear()
    .should('be.empty')
    .type(pUsername)
    .should('have.value', pUsername)

    cy.get('#P9999_PASSWORD')
    .should('be.empty')
    .type(pPassword)
    .should('have.value', pPassword)

    cy.get('#btn-login').click()

    //Country Report
    cy.get(':nth-child(3) > .t-Card > .t-Card-wrap > .t-Card-titleWrap').click()

    //Create Button
    cy.get('[data-cy=create-btn]').click()

    cy.frameLoaded()

    cy.get('iframe')

    cy.iframe().find('[data-cy=country-name]').should('be.visible').type('Hello World')

    cy.iframe().find('[data-cy=create-btn]').should('be.visible').click()
  })

  Cypress.on('uncaught:exception', (err, runnable) => {
    return false
  })
})
